const canvas = document.getElementById('loadout');
const ctx = canvas.getContext('2d');
let b35 = {
  x:null,
  w:100,
  h:18,
  text:'35',
  color:'grey',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '16px helvetica'
    ctx.textAlign='center'
    ctx.fillText('35',this.x+(this.w/2),canvas.height/2+10)
  }
}
let b45 = {
  x:null,
  w:100,
  h:30,
  text:'45',
  color:'grey',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '20px helvetica'
    ctx.textAlign='center'
    ctx.fillText('45',this.x+(this.w/2),canvas.height/2+10)
  }
}
let b55 = {
  x:null,
  w:100,
  h:30,
  text:'55',
  color:'grey',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '20px helvetica'
    ctx.textAlign='center'
    ctx.fillText('55',this.x+(this.w/2),canvas.height/2+10)
  }
}
let p100 = {
  x:null,
  w:50,
  h:300,
  text:'100',
  color:'#ff3d00',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '20px helvetica'
    ctx.textAlign='center'
    ctx.fillText('100',this.x+(this.w/2),canvas.height/2+10)
  }
}
let p45 = {
  x:null,
  w:40,
  h:300,
  text:'45',
  color:'#ffb300',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '20px helvetica'
    ctx.textAlign='center'
    ctx.fillText('45',this.x+(this.w/2),canvas.height/2+10)
  }
}
let p35 = {
  x:null,
  w:35,
  h:210,
  text:'35',
  color:'#4caf50',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '20px helvetica'
    ctx.textAlign='center'
    ctx.fillText('35',this.x+(this.w/2),canvas.height/2+10)
  }
}
let p25 = {
  x:null,
  w:30,
  h:200,
  text:'25',
  color:'#00796b',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '20px helvetica'
    ctx.textAlign='center'
    ctx.fillText('25',this.x+(this.w/2),canvas.height/2+10)
  }
}
let p10 = {
  x:null,
  w:20,
  h:150,
  text:'10',
  color:'#2BBBAD',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '18px helvetica'
    ctx.textalign='center'
    ctx.fillText('10',this.x+(this.w/2),canvas.height/2+10)
  }
}
let p5 = {
  x:null,
  w:15,
  h:125,
  text:'5',
  color:'#4285F4',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '20px helvetica'
    ctx.textalign='center'
    ctx.fillText('5',this.x+(this.w/2),canvas.height/2+10)
  }
}
let p2_5 = {
  x:null,
  w:15,
  h:100,
  text:'2.5',
  color:'#9933CC',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '14px helvetica'
    ctx.textalign='center'
    ctx.fillText('2.5',this.x+(this.w/2),canvas.height/2+10)
  }
}
let p1 = {
  x:null,
  w:10,
  h:80,
  text:'1',
  color:'maroon',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '8px helvetica'
    ctx.textalign='center'
    ctx.fillText('1',this.x+(this.w/2),canvas.height/2+10)
  }
}
let p_5 = {
  x:null,
  w:6,
  h:70,
  text:'.5',
  color:'aquamarine',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '8px helvetica'
    ctx.textalign='center'
    ctx.fillText('.5',this.x+(this.w/2),canvas.height/2+10)
  }
}
let p_25 = {
  x:null,
  w:6,
  h:60,
  text:'.25',
  color:'gold',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '8px helvetica'
    ctx.textalign='center'
    ctx.fillText('.25',this.x+(this.w/2),canvas.height/2+10)
  }
}
let pb = {
  x:null,
  w:35,
  h:300,
  text:'10',
  color:'#00aa00',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '20px helvetica'
    ctx.textAlign='center'
    ctx.fillText('B',this.x+(this.w/2),canvas.height/2+10)
  }
}
let c5 = {
  x:null,
  w:30,
  h:40,
  text:'5',
  color:'grey',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '20px helvetica'
    ctx.textalign='center'
    ctx.fillText('5',this.x+(this.w/2),canvas.height/2+10)
  }
}
let c0 = {
  x:null,
  w:15,
  h:38,
  text:'0',
  color:'grey',
  draw: function(){
    ctx.strokeStyle = 'white'
    ctx.fillStyle = this.color
    ctx.fillRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.strokeRect(this.x,(canvas.height/2)-this.h/2,this.w,this.h)
    ctx.fillStyle = 'black'
    ctx.font = '16px helvetica'
    ctx.textalign='center'
    ctx.fillText('0',this.x+(this.w/2),canvas.height/2+10)
  }
}
let weight = {
  x:null,
  text:'420',
  draw: function(){
    ctx.font='60px helvetica';
    ctx.fillStyle='black'
    ctx.fillText(this.text+' lbs.',this.x+125,canvas.height/2+10)
  }
}
function lift(weight=0,lift=null){
  ctx.clearRect(0,0,canvas.width,canvas.height)
  if(weight<35||lift==null){
    return alert('ERROR: Invalid lift!')
  }
  let bar,collar,loadWeight,bumper;
  let total = [];
  if(lift=='squat'&&weight<45){
    bar=35;
    collar=0
  }
  if(lift=='squat'&&weight>=45&&weight<55){
    bar=45;
    collar=0;
  }
  if(lift=='squat'&&weight>=55&&weight<65){
    bar=55;
    collar=0;
  }
  if(lift=='squat'&&weight>=65){
    bar=55;
    collar=5
  }
  if(lift=='benchpress'&&weight<45){
    bar=35;
    collar=0;
  }
  if(lift=='benchpress'&&weight<65){
    bar=45;
    collar=0;
  }
  if(lift=='benchpress'&&weight>=65){
    bar=45;
    collar=5;
  }
  if(lift=='deadlift'&&weight<145){
    bar=45;
    bumper=true;
    collar=5;
  }
  if(lift=='deadlift'&&weight>=145){
    bar=45;
    bumper=false;
    collar=5;
  }
  if(bar=='35') total.push(b35);
  if(bar=='45') total.push(b45);
  if(bar=='55') total.push(b55);
  loadWeight = (weight-bar-(2*collar))/2;
  if(bumper){
    total.push(pb);
    loadWeight-=10
  }
  while(loadWeight>0){
    if(weight>=600&&loadWeight-100>=0){
      total.push(p100)
      loadWeight-=100
    }
    else if(loadWeight-45>=0){
      total.push(p45)
      loadWeight-=45
    }
    else if(loadWeight-25>=0){
      total.push(p25)
      loadWeight-=25
    }
    else if(loadWeight-10>=0){
      total.push(p10)
      loadWeight-=10
    }
    else if(loadWeight-5>=0){
      total.push(p5)
      loadWeight-=5
    }
    else if(loadWeight-2.5>=0){
      total.push(p2_5)
      loadWeight-=2.5
    }
    else if(loadWeight-1>=0){
      total.push(p1)
      loadWeight-=1
    }
    else if(loadWeight-.5>=0){
      total.push(p_5)
      loadWeight-=.5
    }
    else if(loadWeight-.25>=0){
      total.push(p_25)
      loadWeight-=.25
    }
  }
  if(collar=='0') total.push(c0);
  if(collar=='5') total.push(c5);
  for(let i=0;i<total.length;i++){
    if(i==0){
      total[i].x = 0
      total[i].draw()
    }
    else{
      total[i].x = total[i-1].x+(total[i-1].w)+5
      total[i].draw()
    }
  }
  return `${lift}: ${weight}`
}

$('#print').click(()=>{
  lift($('#weight').val(),$('#lift').val())
})
